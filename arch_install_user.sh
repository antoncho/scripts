printf '\033c'

cd /opt/yay
makepkg -si

cd $HOME
mkdir $HOME/.suckless
cd $HOME/.suckless
git clone https://gitlab.com/antoncho/scripts.git
git clone https://gitlab.com/antoncho/dwm.git
git clone https://gitlab.com/antoncho/dmenu.git
git clone https://gitlab.com/antoncho/st.git
sudo make -C ./dwm clean install
sudo make -C ./dmenu clean install
sudo make -C ./st clean install
cd $HOME
rm .xinitrc
touch .xinitrc
echo "$HOME/.suckless/scripts/./autostart.sh" >> .xinitrc

yay -S brave-bin viber zoom nodejs-lts-erbium npm spotify webtorrent-cli heroic-games-launcher-bin multimc-bin vmware-workstation catppuccin-gtk-theme

mkdir .discord
cd .discord
wget "https://discord.com/api/download/canary?platform=linux&format=tar.gz"
tar -xf canary\?platform\=linux\&format\=tar.gz
rm canary\?platform\=linux\&format\=tar.gz
git clone https://github.com/powercord-org/powercord
cd ./powercord
sudo npm i
mkdir -p $HOME/.discord/powercord/src/Powercord/themes
cd $HOME/.discord/powercord/src/Powercord/themes
git clone https://github.com/catppuccin/discord.git
cd $HOME

lsblk
read -p "Enter backup drive ...again: [/dev/...] " backup2
read -p "Username...again: " usrname
sudo mount $backup2 /mnt
sudo cp -r /mnt/HOME/Documents $HOME
sudo chown -R $usrname:$usrname Documents
sudo cp -r /mnt/HOME/Downloads $HOME
sudo chown -R $usrname:$usrname Downloads
sudo cp -r /mnt/HOME/Pictures $HOME
sudo chown -R $usrname:$usrname Pictures
mkdir -p $HOME/.config/gtk-2.0
mkdir -p $HOME/.config/gtk-3.0
sudo cp -r /mnt/HOME/config/gtk-2.0/settings.ini $HOME/.config/gtk-2.0
sudo chown -R $usrname:$usrname .config/gtk-2.0
sudo cp -r /mnt/HOME/config/gtk-3.0/settings.ini $HOME/.config/gtk-3.0
sudo chown -R $usrname:$usrname .config/gtk-3.0
mkdir -p $HOME/.local/share/multimc/instances
sudo cp -r /mnt/HOME/local/share/multimc/instances/1.18.2 $HOME/.local/share/multimc/instances/
sudo chown -R $usrname:$usrname .local/share/multimc/instances/
mkdir -p $HOME/.config/BraveSoftware/Brave-Browser/
sudo cp -r /mnt/HOME/Default $HOME/.config/BraveSoftware/Brave-Browser/
sudo chown -R $usrname:$usrname $HOME/.config/BraveSoftware/Brave-Browser/Default
sudo cp /mnt/USR/local/bin/* /usr/local/bin
sudo chmod +x /usr/local/bin/*
sudo mkdir -p /etc/X11/xorg.conf.d
sudo mkdir -p /etc/pacman.d/hooks
sudo cp /mnt/ETC/X11/xorg.conf.d/* /etc/X11/xorg.conf.d
sudo cp /mnt/ETC/pacman.d/hooks/nvidia.hook /etc/pacman.d/hooks
echo "startx" >> .bash_profile

exit
