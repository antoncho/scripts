#/bin/sh

action1=$(echo "settings power" | sed 's/ /\n/g' | dmenu -i -l 2 -nb "#161320" -nf "#988BA2" -sb "#1E1E2E" -sf "#C3BAC6")

case "$action1" in
	'settings')
		action2=$(echo "audio bluetooth network brightness" | sed 's/ /\n/g' | dmenu -i -l 4 -nb "#161320" -nf "#988BA2" -sb "#1E1E2E" -sf "#C3BAC6")
		case "$action2" in
			'audio')
				pavucontrol
			;;
			'bluetooth')
				st "bluetoothctl"
			;;
			'network')
				st "nmtui"
			;;
			'brightness')
				bri=$(echo "" | dmenu -i -l 1 -nb "#161320" -nf "#988BA2" -sb "#1E1E2E" -sf "#C3BAC6")
				brightnessctl s $bri
			;;
		esac
	;;
	'power')
		power=$(echo "Shutdown Reboot Hibernate KillX" | sed 's/ /\n/g' | dmenu -i -l 4 -nb "#161320" -nf "#988BA2" -sb "#1E1E2E" -sf "#C3BAC6")

		case "$power" in
		        'Shutdown')
             			shutdown now
		        ;;
		        'Reboot')
		                reboot
		        ;;
		        'Hibernate')
		                systemctl hibernate
		        ;;
		        'KillX')
	                	kill $(pidof Xorg)
        		;;
		esac
        ;;
esac
