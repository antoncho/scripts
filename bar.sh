#!/bin/sh
# ^c$var^ = fg color
# ^b$var^ = bg color

# load colors
. ~/.suckless/scripts/catppuccin.theme

cpu() {
	cpu_val=$(grep -o "^[^ ]*" /proc/loadavg)

	printf "^c$black0^^b$lavender^ CPU "
	printf "^c$lavender^^b$black0^ $cpu_val"
}
mem(){
	printf "^c$black0^^b$white^ RAM "
	printf "^c$white^^b$black0^ $(free -h | awk '/^Mem/ { print $3 }' | sed s/i//g)"
}

clock() {
	printf "^c$black0^^b$rosewater^ $(date '+%I:%M') "
}
battery() {
	printf "^c$gray2^^b$black0^ $(cat /sys/class/power_supply/BAT0/capacity)"
	printf "%%"
}
player() {
	playerctl -p spotify status | grep -q 'Playing' && printf "^c#ABE9B3^^b$black0^ $(playerctl -p spotify metadata xesam:title)"
}
bluetooth() {
	if bluetoothctl show | grep -q 'Powered: yes'; then
		if bluetoothctl info | grep -q 'Connected: yes';then 
			printf "^c$black0^^b#ABE9B3^ BT"
		else
			printf "^c$black0^^b$gray2^ BT"
		fi
	else
		printf "^c$black0^^b$black4^ BT"
	fi
}
while true; do
	sleep 1 && xsetroot -name "$(player) $(bluetooth) $(battery) $(mem) $(cpu) $(clock)"
done
