#!/bin/sh

#NVIDIA Proprietary magic
xrandr --setprovoderoutputsource modesetting NVIDIA-0 &
xrandr --auto &
brightnessctl s 0 &

#no idea
picom &
dunst &

#wallpaper
nitrogen --restore &

#autostart apps
powercord &
bluetoothctl power on

#dwm and bar
$HOME/.suckless/scripts/./bar.sh &
exec dwm
