#part1
printf '\033c'
echo "Arch-chan instalator v1.0"
sed -i "s/^#ParallelDownloads = 5$/ParallelDownloads = 15/" /etc/pacman.conf
loadkeys us
timedatectl set-ntp true
read -p "Have you made linux filesystem partition before install? [y/n] " has_linux_part
if [[ $has_linux_part = y ]] ; then
	lsblk
	read -p "Select the linux fs partition: [/dev/...] " linux_fs
	mkfs.ext4 $linux_fs
	mount $linux_fs /mnt
fi
if [[ $has_linux_part = n ]] ; then
	echo "Please make one before you run the script"
	exit
fi
read -p "Enter EFI partion: [/dev/...] " efipartition
read -p "Do you want it to be formated? [y/n] " answer
if [[ $answer = y ]] ; then
	mkfs.fat -F 32 $efipartition
fi
mkdir -p /mnt/boot
mount $efipartition /mnt/boot
pacstrap /mnt base base-devel linux linux-firmware
genfstab -U /mnt >> /mnt/etc/fstab

read -p "Enter backup drive [/dev/...] " bckup
mount $bckup /mnt/mnt
cp /mnt/mnt/arch_install_sysroot.sh /mnt
umount $bckup
umount $efipartition
chmod +x /mnt/arch_install_sysroot.sh
arch-chroot /mnt
exit 
