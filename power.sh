#/bin/sh
menu="[Shutdown]
[Reboot]
[Hibernate]
[KillX]
"
action=$(echo -e "$menu" | dmenu -i -nb "#161320" -nf "#988BA2" -sb "#1E1E2E" -sf "#C3BAC6")

case "$action" in
	'[Shutdown]')
		shutdown now
	;;
	'[Reboot]')
                reboot
        ;;
	'[Hibernate]')
                systemctl hibernate
        ;;
	'[KillX]')
		kill $(pidof Xorg)
	;;
esac
