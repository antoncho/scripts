printf '\033c'

lsblk
read -p "Enter backup drive: [/dev/...] " backup
mount $backup /mnt
read -p "Enter EFI drive: [/dev/...] " efipart
mount $efipart /boot
pacman --noconfirm -S sed git
sed -i "/\[multilib\]/,/Include/"'s/^#//' /etc/pacman.conf
sed -i "s/^#ParallelDownloads = 5$/ParallelDownloads = 15/" /etc/pacman.conf
ln -sf /usr/share/zoneinfo/Europe/Sofia /etc/localtime
hwclock --systohc
echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
echo "de_DE.UTF-8 UTF-8" >> /etc/locale.gen
echo "bg_BG.UTF-8 UTF-8" >> /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" > /etc/locale.conf
echo "KEYMAP=us" > /etc/vconsole.conf
echo "arch_linux" > /etc/hostname
echo "127.0.0.1       localhost" >> /etc/hosts
echo "::1             localhost" >> /etc/hosts
sed -i 's/HOOKS=(/HOOKS=(resume /' /etc/mkinitcpio.conf
mkinitcpio -P
passwd

mount $backup /mnt
mount $efipart /boot
pacman --noconfirm -Sy grub efibootmgr os-prober
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB
mkdir -p /usr/share/grub/themes/
cp -r /mnt/USR/share/grub/themes/catppuccin-grub-theme /usr/share/grub/themes/
echo "GRUB_THEME=\"/usr/share/grub/themes/catppuccin-grub-theme/theme.txt\"" >> /etc/default/grub
sed -i 's/^#GRUB_DISABLE_OS_PROBER=false$/GRUB_DISABLE_OS_PROBER=false/' /etc/default/grub
sed -i 's/GRUB_TIMEOUT=5/GRUB_TIMEOUT=0/' /etc/default/grub
sed -i 's/GRUB_CMDLINE_LINUX_DEFAULT="/GRUB_CMDLINE_LINUX_DEFAULT=" nouveau.modeset=0 resume=UUID=9e5c048b-45ad-44e7-ae93-31efdd14db04 /' /etc/default/grub
grub-mkconfig -o /boot/grub/grub.cfg

pacman --noconfirm -Sy pulseaudio pulseaudio-alsa xorg-server xorg-xinit xorg-xsetroot xorg-xprop neofetch atop htop mpv mupdf p7zip brightnessctl dosfstools ntfs-3g picom nitrogen figlet ponysay nvidia wget neovim libnotify dunst dhcpcd networkmanager pavucontrol steam libx11 libxinerama libxft webkit2gtk sudo noto-fonts noto-fonts-emoji noto-fonts-cjk libxft picom libreoffice-still libreoffice-still-bg libreoffice-still-de playerctl pcmanfm baobab gparted bluez bluez-utils pulseaudio-bluetooth transmission-gtk scrot mupdf-tools gnome-themes-extra gtk-engine-murrine linux-headers

pacman --noconfirm -Sy wine-staging winetricks

pacman --noconfirm -Sy giflib lib32-giflib libpng lib32-libpng libldap lib32-libldap gnutls lib32-gnutls mpg123 lib32-mpg123 openal lib32-openal v4l-utils lib32-v4l-utils libpulse lib32-libpulse alsa-plugins lib32-alsa-plugins alsa-lib lib32-alsa-lib libjpeg-turbo lib32-libjpeg-turbo libxcomposite lib32-libxcomposite libxinerama lib32-libxinerama ncurses lib32-ncurses opencl-icd-loader lib32-opencl-icd-loader libxslt lib32-libxslt libva lib32-libva gtk3 lib32-gtk3 gst-plugins-base-libs lib32-gst-plugins-base-libs vulkan-icd-loader lib32-vulkan-icd-loader cups samba dosbox

pacman --noconfirm -Sy nvidia-dkms nvidia-utils lib32-nvidia-utils nvidia-settings vulkan-icd-loader lib32-vulkan-icd-loader

echo "%wheel ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
echo "Enter Username: "
read username
useradd -m -G wheel $username
passwd $username

systemctl enable NetworkManager.service 
echo "/dev/sda2       /home/toncho/hdd        ext4    rw,relatime     0 2" >> /etc/fstab

cd /opt
sudo git clone https://aur.archlinux.org/yay.git
sudo chown -R $username:users /opt/yay
cd /home/$username

mkdir hdd
cp /mnt/arch_install_graphical.sh .
chown $username:$username arch_install_graphical.sh
chmod +x arch_install_graphical.sh
cp /mnt/arch_install_user.sh .
chown $username:$username arch_install_user.sh
chmod +x arch_install_user.sh
exit 
