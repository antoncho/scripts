rm -r /mnt/HOME
rm -r /home/toncho/hdd/WinBackup
rm -r /mnt/USR
rm -r /mnt/ETC
rm -r /mnt/arch*
echo "Removed old backup"
mkdir -p /mnt/HOME
cp -r /home/toncho/Documents /mnt/HOME
echo "Backed up Documents"
cp -r /home/toncho/Downloads /mnt/HOME
echo "Backed up Downloads"
cp -r /home/toncho/Pictures /mnt/HOME
echo "Backed up Pictures"
cp -r /home/toncho/.config/BraveSoftware/Brave-Browser/Default /mnt/HOME
echo "Backed up Brave data"
mkdir -p /mnt/HOME/config/gtk-2.0
mkdir -p /mnt/HOME/config/gtk-3.0
cp -r /home/toncho/.config/gtk-2.0/settings.ini /mnt/HOME/config/gtk-2.0/
cp -r /home/toncho/.config/gtk-3.0/settings.ini /mnt/HOME/config/gtk-3.0/
echo "Backed up gtk settings"
mkdir -p /mnt/HOME/local/share/multimc/instances/
cp -r /home/toncho/.local/share/multimc/instances/1.18.2 /mnt/HOME/local/share/multimc/instances/
echo "Backed up mc world"
mkdir /home/toncho/hdd/WinBackup
cp -r /home/toncho/.vmware/Windows11 /home/toncho/hdd/WinBackup
mkdir -p /mnt/USR/local/bin
cp /usr/local/bin/notflix /mnt/USR/local/bin
cp /usr/local/bin/powercord /mnt/USR/local/bin
cp /usr/local/bin/gitcommit /mnt/USR/local/bin
cp /usr/local/bin/vm /mnt/USR/local/bin
cp /usr/local/bin/vm-stop /mnt/USR/local/bin
echo "Backed up usr/local/bin"
mkdir -p /mnt/ETC/X11/xorg.conf.d
cp /etc/X11/xorg.conf.d/* /mnt/ETC/X11/xorg.conf.d
echo "Backed up X11 config"
mkdir -p /mnt/ETC/pacman.d/hooks
cp /etc/pacman.d/hooks/nvidia.hook /mnt/ETC/pacman.d/hooks
echo "Backed up nvidia pacman hook"
mkdir -p /mnt/USR/share/grub/themes/
cp -r /usr/share/grub/themes/catppuccin-grub-theme /mnt/USR/share/grub/themes
echo "Backed up catppuccin grub theme"
cp /home/toncho/.suckless/scripts/arch_install* /mnt
chmod +x /mnt/arch_install*
echo "Backed up archinstall scripts"
